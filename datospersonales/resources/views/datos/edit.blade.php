@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{route('datos.update',$dato->id)}}" method="POST">
{{method_field('PATCH')}}
@csrf
    <div class="row justify-content-center form-group">
        <div class="col-md-8">
        <h1>Editar Datos Personales</h1>
        <br>
        <h3>Nombre</h3>
        <input type="text" name="nombre" value="{{$dato->nombre}}" class="form-control">
        <h4>Apellido Paterno</h4>
        <input type="text" name="apellidopaterno" value="{{$dato->apellidopaterno}}" class="form-control">
        <h4>Apellido Materno</h4>
        <input type="text" name="apellidomaterno" value="{{$dato->apellidomaterno}}" class="form-control">
        <h4>Fecha de Nacimiento</h4>
        <input type="text" name="fechadenacimiento" value="{{$dato->fechadenacimiento}}" class="form-control">
        <br>
        <input type="submit" value="Guardar" class="btn btn-primary">
            
        </div>
    </div>
    </form>
</div>
@endsection