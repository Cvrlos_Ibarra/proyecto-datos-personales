@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h1>Datos Personales</h1>
        <br>

        <table class="table table-bordered">
          <thead class="thead-dark">
          <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>APELLIDO PATERNO</th>
            <th>APELLIDO MATERNO</th>
            <th>NACIMIENTO</th>
            <th>ACCIONES</th>           
          
          </tr>
          </thead>
        @foreach($dato as $da)

          <tr>
            <td>{{$da->id}}</td>
            <td>{{$da->nombre}}</td>
            <td>{{$da->apellidopaterno}}</td>
            <td>{{$da->apellidomaterno}}</td>
            <td>{{$da->fechadenacimiento}}</td>
            <td>
              <a href="{{url('/datos/'.$da->id.'/edit')}}" class="btn btn-primary" >Editar</a>
              
              @include('datos.delete',['$dato' => $dato])
            </td>
          
          </tr>

        @endforeach
        
        </table>
            
        </div>
        <form action="{{route('datos.create')}}" method="GET">
          <input type="submit" value="Nuevo" class="btn btn-success">
        </form>
    </div>
</div>
@endsection

